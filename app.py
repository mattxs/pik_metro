from flask import Flask, request, jsonify

from metro import Metro

app = Flask(__name__)


@app.route('/api/v1/metro/verificate/', methods=['POST'])
def verificate():
    unchanged, updated, deleted = [], [], []
    if request.method == 'POST':
        post_metro_list = request.get_json(force=True)
        m = Metro(post_stations=post_metro_list)

        hh_stations = m.stations

        unchanged = list(set(post_metro_list) & set(hh_stations))
        updated = list(set(post_metro_list) - set(hh_stations))
        deleted = list(set(hh_stations) - set(post_metro_list))
    return jsonify({
        'unchanged': unchanged,
        'updated': updated,
        'deleted': deleted
    })


if __name__ == '__main__':
    app.run(host='localhost:5000', debug=True)
