from requests import Session


class Metro:
    METRO_URL = 'https://api.hh.ru/metro/1'

    def __init__(self, post_stations: list):
        self.post_stations = post_stations
        self.ses: Session = Session()

    def get_stations(self) -> dict:
        data = None
        request = self.ses.get(self.METRO_URL)
        if request.status_code == 200:
            data = request.json()
        return data

    @property
    def stations(self) -> list:
        s = []
        metro_data = self.get_stations()
        for lines in metro_data['lines']:
            for station in lines['stations']:
                if station['name'] not in s:
                    s.append(station['name'])
        return s
